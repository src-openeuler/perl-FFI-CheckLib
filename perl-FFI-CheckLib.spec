%global _empty_manifest_terminate_build 0
Name:           perl-FFI-CheckLib
Version:        0.31
Release:        2
Summary:        FFI::CheckLib Perl module
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            https://metacpan.org/release/FFI-CheckLib
Source0:        https://cpan.metacpan.org/authors/id/P/PL/PLICEASE/FFI-CheckLib-%{version}.tar.gz
BuildArch:      noarch

BuildRequires:  make
BuildRequires:  perl-generators
BuildRequires:  perl-interpreter
BuildRequires:  perl(:VERSION) >= 5.6
BuildRequires:  perl(Test2::V0) >= 0.000060
BuildRequires:  perl(File::Which)
 
%description
This Perl module checks whether a particular dynamic library is available for
Foreign Function Interface (FFI) to use. It is modeled heavily on
Devel::CheckLib, but will find dynamic libraries even when development
packages are not installed. It also provides a find_lib function that will
return the full path to the found dynamic library, which can be feed directly
into FFI::Platypus or FFI::Raw.

%package help
Summary:  FFI::CheckLib Perl module
Provides: perl-FFI-CheckLib-doc

%description help
This Perl module checks whether a particular dynamic library is available for
Foreign Function Interface (FFI) to use. It is modeled heavily on
Devel::CheckLib, but will find dynamic libraries even when development
packages are not installed. It also provides a find_lib function that will
return the full path to the found dynamic library, which can be feed directly
into FFI::Platypus or FFI::Raw.

%prep
%setup -q -n FFI-CheckLib-%{version}
 
%build
export PERL_MM_OPT=""
%{__perl} Makefile.PL INSTALLDIRS=vendor NO_PACKLIST=1 NO_PERLLOCAL=1
%{make_build}
 
%install
export PERL_MM_OPT=""
rm -rf $RPM_BUILD_ROOT

%{make_install}

find $RPM_BUILD_ROOT -type f -name .packlist -exec rm -f {} \;
find $RPM_BUILD_ROOT -depth -type d -exec rmdir {} 2>/dev/null \;

%{_fixperms} $RPM_BUILD_ROOT/*

%check
make test
 
%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc Changes META.json README
%{perl_vendorlib}/*

%files help
%{_mandir}/*

%changelog
* Tue Jul 25 2023 xu_ping <707078654@qq.com> - 0.31-2
- fix build test error

* Wed Jul 19 2023 leeffo <liweiganga@uniontech.com> - 0.31-1
- upgrade to version 0.31

* Tue Jul 20 2021 Xu Jin <jinxu@kylinos.cn> - 0.28-1
- Update package to 0.28

* Mon Aug 3 2020 dingyue <dingyue5@huawei.com> -0.27 -1
- Package init
